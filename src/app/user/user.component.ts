import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';
import { UserApiService } from '../services/user-api.service';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  datas:any[]=[]
  enteredValue:string="";
  // options: string[] = ['Filter with Odd Ids', 'Filter with Even Ids'];
  selectedOptions: string="";

  constructor(private dialog:MatDialog,private userApiService:UserApiService){}

  ngOnInit(): void {
    this.getAllUser();
  }

  getAllUser(){
    this.userApiService.getUsers().subscribe({
      next:(res)=>{
        this.datas=res
      },
      error:()=>{
        alert("Error While fetching data !")
      }
    })
  }

  clearSelection(){
    this.selectedOptions = "";
  }

  openDialog(){
    this.dialog.open(DialogComponent,{
      width:'30%'
    }).afterClosed().subscribe(val =>{
      if(val == 'save'){
        this.getAllUser();
      }
    })
  }

  editUser(userdata:any){
    this.dialog.open(DialogComponent,{
      width:'30%',
      data:userdata 
    }).afterClosed().subscribe(val =>{
      if(val == 'update'){
        this.getAllUser();
      }
    })
  }

  delUser(id:number){
    
    this.userApiService.deleteUser(id).subscribe({
      next:(res:any)=>{
        alert("Delete user successfully");
        this.getAllUser();
      },
      error:()=>{
        alert("Error While delete user !")
      }

    })
    
  }
  

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(!args ){
      return value;
    }
    // if(args2){
      
    // }
    return value.filter((item:any)=>item.name.toLocaleLowerCase().indexOf(args.toLocaleLowerCase()) > -1 || item.address.zipcode.indexOf(args) > -1);
  }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtering'
})
export class FilteringPipe implements PipeTransform {

  transform(value: any, args: any): any {
    if(!args){
      return value;
    }
    return value.filter((item:any)=>{
      if (item.id%2!= 0) {
      return value;
    }});

    
  }

}

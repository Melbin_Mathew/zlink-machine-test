import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserApiService {

  private apiUrl = 'http://localhost:3000/users/';

  constructor(private http:HttpClient) { }
  postUser(data:any){
    return this.http.post<any>("http://localhost:3000/users/",data);
  }
  getUsers(){
    return this.http.get<any>("http://localhost:3000/users/");
  }
  putUser(data:any,id:number){
    return this.http.put<any>("http://localhost:3000/users/"+id,data)
  }
  deleteUser(id: number): Observable<any> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.delete(url);
  }
}

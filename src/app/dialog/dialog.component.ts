import { Component, Inject, OnInit, inject } from '@angular/core';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { UserApiService } from '../services/user-api.service';
import { MatDialogRef,MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  userForm!:FormGroup;
  btnAction:string="Save";
  constructor(private formBuilder:FormBuilder,
    private userApiService:UserApiService,
    @Inject(MAT_DIALOG_DATA) public editUserData:any,
    private dialog:MatDialogRef<DialogComponent>){}
  ngOnInit(): void {
    
    this.userForm = this.formBuilder.group({
      userName:['',Validators.required],
      userId:['',Validators.required],
      street:['',Validators.required],
      city:['',Validators.required],
      zipCode:['',Validators.required]
      
    })
    console.log(this.editUserData);
    
    if(this.editUserData){
      this.btnAction="Update"
      this.userForm.controls['userName'].setValue(this.editUserData.name);
      this.userForm.controls['userId'].setValue(this.editUserData.id);
      this.userForm.controls['street'].setValue(this.editUserData.address.street);
      this.userForm.controls['city'].setValue(this.editUserData.address.city);
      this.userForm.controls['zipCode'].setValue(this.editUserData.address.zipcode);
    }
  }

  updateUser(){
    let data = {
      "id": this.userForm.value.userId,
      "name": this.userForm.value.userName,
      "address": {
        "street": this.userForm.value.street,
        "city": this.userForm.value.city,
        "zipcode": this.userForm.value.zipCode
      }   
    }
    this.userApiService.putUser(data,this.editUserData.id).subscribe({
      next:(res)=>{
        alert("User Update Successfully ");
        this.userForm.reset();
        this.dialog.close('update');
      },
      error:() => {
        alert("Error while Updating the User !")
      }
    })
  }

  addUser(){
    if(!this.editUserData){
      if(this.userForm.valid){
        let data = {
          "id": this.userForm.value.userId,
          "name": this.userForm.value.userName,
          "address": {
            "street": this.userForm.value.street,
            "city": this.userForm.value.city,
            "zipcode": this.userForm.value.zipCode
          }   
        }
        this.userApiService.postUser(data).subscribe({
          next:(res)=>{
            alert("User Added Successfully ");
            this.userForm.reset();
            this.dialog.close('save');
          },
          error:()=>{
            alert("Error while adding the User !")
          }
        })
      } 
    }else{
      this.updateUser()
    }
  }



  
  

}
